module.exports = {
	globDirectory: 'public/',
	globPatterns: [
		'**/*.{eot,svg,ttf,woff,woff2,png,mp4,jpg,psd,ai,txt,js,css,ico,html}'
	],
	swDest: 'public/sw.js',
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/
	]
};