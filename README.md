# Hi, I'm Juvar Abrera 👋

## How to build
1. `npm install`
2. `npm install -g gulp-cli`
3. `gulp build`
4. Run `public` folder in a localhost \
a. Using python: `pip install http.server && python -m http.server`

## How to generate service worker
1. `npx workbox generateSW ./workbox-config.js`