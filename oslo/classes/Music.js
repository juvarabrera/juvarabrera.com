class Music {
    constructor() {
        this.music = null;
        this.max_portfolio_show_per_load = 5;
        this.container = document.querySelector(".music-container");
        this.template = this.container.querySelector("[data-template]");
    }
    async loadData() {
        if(this.music != null) return
        this.music = await fetch(`${JuvarAbrera.API_ROOT_URL}/music/all`).then(function(response) {
            return response.json()
        }).then(function(data) {
            return data
        });
        this.container.querySelectorAll(".loading").forEach(loading => {
            loading.remove();
        })
        let views_count = 0;
        for(let i in this.music) {
            let musicItem = this.music[i];
            var clone = this.template.cloneNode(true);
            clone.querySelector("h3").innerText = musicItem.title;
            clone.querySelector("span").innerText = musicItem.views + " views"
            clone.querySelector(".portfolio-ss").style.backgroundImage = `url(${musicItem.thumbnail})`;
            clone.querySelector("a").setAttribute("href", musicItem.url)
            // for(let j in musicItem.topics) {
            //     let topic = musicItem.topics[j];
            //     clone.querySelector(".tags").insertAdjacentHTML("beforeend", `<span class="tag">${topic.name}</span>`)
            // }
            clone.removeAttribute("data-template");
            if(i >= this.max_portfolio_show_per_load) {
                clone.classList.add("d-none");
            }
            views_count += musicItem.views
            this.container.insertAdjacentElement("beforeend", clone);
        }
        document.getElementById("num_videos").setAttribute("data-animate-number-to", this.music.length);
        document.getElementById("num_views").setAttribute("data-animate-number-to", views_count);
    }
}