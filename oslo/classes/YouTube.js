class YouTube {
    constructor() {
        this.container = document.getElementById("youtube_live");
        this.checkIfLive();
    }
    async checkIfLive() {
        let video = await this.getLiveVideo();
        if(video.length === 1) {
            this.container.style.display = "block";
            this.container.querySelector(".title").textContent = video[0].title.runs[0].text;
            this.container.querySelector("img").src = video[0].thumbnail.thumbnails[3].url;
            this.container.href = `https://www.youtube.com/watch?v=${video[0].videoId}`
        }
    }
    async getLiveVideo() {
        return await fetch(`${JuvarAbrera.API_ROOT_URL}/youtube/is_live`).then(function(response) {
            return response.json();
        });
    }
}