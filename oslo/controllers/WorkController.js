Oslo.Controller["WorkController"] = {
	"construct": function(parameters) {

	},
	"index": function(parameters) {
		mixpanel.track("Work");
		return Oslo.View.render(async () => {
      let portfolio = new Portfolio();
      portfolio.loadData();
      let miniProject = new MiniProject();
      miniProject.loadData();
      let git = new Git(2015);
      git.loadData();
      const calculateYearsExperience = function() {
        const targetDate = new Date('2016-10-13');
        const currentDate = new Date();
        const years = Math.floor((currentDate - targetDate) / (1000 * 60 * 60 * 24 * 365.25));
        document.getElementById("num_years").setAttribute("data-animate-number-to", years);
      }
      let clockify = new Clockify();
      await clockify.start();
      await clockify.loadReport();
      calculateYearsExperience();
    });
	},
	"setup": function(parameters) {
		mixpanel.track("Work Setup");
		return Oslo.View.render();
	},
	"git-art-planner": function(parameters) {
		mixpanel.track("Git Art Planner");
		return Oslo.View.render();
	}
}