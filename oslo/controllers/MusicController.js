Oslo.Controller["MusicController"] = {
	"construct": function(parameters) {

	},
	"index": function(parameters) {
		mixpanel.track("Music");
		return Oslo.View.render(async () => {
      let music = new Music();
      music.loadData();
    });
	},
	"unravel": function(parameters) {
		mixpanel.track("Unravel by Jarrell Maverick Remulla");
		return Oslo.View.render();
	}
}